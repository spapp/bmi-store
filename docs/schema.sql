

CREATE TABLE `user` (
	`id`	        INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	`first_name`	TEXT CHECK( LENGTH ( `first_name` ) > 0 AND LENGTH ( `first_name` ) < 256 ),
	`last_name`	    TEXT CHECK( LENGTH ( `first_name` ) > 0 AND LENGTH ( `last_name` ) < 256 ),
	`age`	        INTEGER NOT NULL CHECK( `age` > 0 AND `age` < 150 ),
	`sex`	        TEXT CHECK(`sex` == 'male' OR `sex` == 'female') -- ENUM ('male', 'female')
);

CREATE TABLE `bmi_data` (
	`id`	    INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	`user_id`	INTEGER NOT NULL,
	`weight`	INTEGER NOT NULL CHECK(`weight` > 0 AND `weight` < 500), -- in kg
	`height`	INTEGER NOT NULL CHECK(`height` > 0 AND `height` < 300), -- in cm
	`created`	TEXT NOT NULL,

	FOREIGN KEY(`user_id`) REFERENCES `user` ( `id` ) ON DELETE CASCADE
);

