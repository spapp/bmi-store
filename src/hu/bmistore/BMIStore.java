/**
 * @author Sándor Papp <spapp@spappsite.hu>
 * @copyright 2015
 * @license http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @link https://bitbucket.org/spapp
 * @package hu.bmistore
 * @project bmi_store
 * @since 2015.05.14.
 */
package hu.bmistore;

import hu.database.SQLite;
import hu.bmistore.database.BMIDataRepository;
import hu.bmistore.database.UserRepository;
import hu.bmistore.view.MainMenu;

import java.io.*;

public class BMIStore {

    public static final String ENCODING = "UTF-8";

    public BMIStore(File dbFile) throws IOException {
        try {
            SQLite db = new SQLite(dbFile.getAbsolutePath());
            UserRepository.getInstance().setAdapter(db);
            BMIDataRepository.getInstance().setAdapter(db);

            BufferedReader input = new BufferedReader(new InputStreamReader(System.in, BMIStore.ENCODING));

            new MainMenu(input, System.out);

        } catch (UnsupportedEncodingException exc) {
            System.err.println(exc.getMessage());
        } catch (IOException exc) {
            System.err.println(exc.getMessage());
        } catch (Exception exc) {
            System.err.println(exc.getMessage());
        }
    }
}
