/**
 * @author Sándor Papp <spapp@spappsite.hu>
 * @copyright 2015
 * @license http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @link https://bitbucket.org/spapp
 * @package hu.bmistore
 * @project bmi_store
 * @since 2015.05.14.
 */
package hu.bmistore;

import java.io.File;
import java.io.IOException;

public class Main {
    public static void main (String[] args) throws IOException {
        File dbFile = new File("resources/bmi_data.sqlite");

        new BMIStore(dbFile);
    }
}
