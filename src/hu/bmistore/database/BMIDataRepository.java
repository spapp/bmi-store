/**
 * @author Sándor Papp <spapp@spappsite.hu>
 * @copyright 2015
 * @license http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @link https://bitbucket.org/spapp
 * @package hu.bmistore.model
 * @project bmi_store
 * @since 2015.05.14.
 */
package hu.bmistore.database;

import hu.database.Table;
import hu.bmistore.model.BMIData;

public class BMIDataRepository extends Table {

    protected static BMIDataRepository instance;

    protected BMIDataRepository() {
        super();

        this.setName("bmi_data");
        this.setFields("id, user_id, weight, height, created");
        this.setRecordModel("hu.bmistore.model.BMIData");
    }

    /**
     * Finds a bmi data by id
     *
     * @param id
     *
     * @return
     */
    @Override
    public BMIData findById(int id) {
        return (BMIData) super.findById(id);
    }

    /**
     * Inserts a new bmi data
     *
     * @param bmiData
     * @return
     */
    public boolean insert(BMIData bmiData) {
        String sql = String.format(
                "INSERT INTO %s (%s) VALUES (null, %d, %d, %d, '%s');",
                this.getName(),
                this.getFields(),
                bmiData.getUserId(),
                bmiData.getWeight(),
                bmiData.getHeight(),
                bmiData.getCreatedAsString()
        );

        return this.adapter.execute(sql);
    }
    /**
     * Deletes a bmi data
     *
     * @param user
     * @return
     */
    public boolean delete(BMIData bmiData) {
        return this.delete(bmiData.getId());
    }

    /**
     * Updates a bmi data
     *
     * @param bmiData
     * @return
     */
    public boolean update(BMIData bmiData) {
        String sql = String.format(
                "UPDATE %s SET user_id=%d, weight=%d, height=%d, created='%s' WHERE id=%d",
                this.getName(),
                bmiData.getUserId(),
                bmiData.getWeight(),
                bmiData.getHeight(),
                bmiData.getCreated(),
                bmiData.getId()
        );

        return this.adapter.execute(sql);
    }
    /**
     * Returns a new instance
     *
     * @return
     */
    public static BMIDataRepository getInstance() {
        if (null == BMIDataRepository.instance) {
            BMIDataRepository.instance = new BMIDataRepository();
        }

        return BMIDataRepository.instance;
    }
}
