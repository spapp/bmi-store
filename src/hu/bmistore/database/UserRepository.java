/**
 * @author Sándor Papp <spapp@spappsite.hu>
 * @copyright 2015
 * @license http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @link https://bitbucket.org/spapp
 * @package hu.bmistore.model
 * @project bmi_store
 * @since 2015.05.14.
 */
package hu.bmistore.database;

import hu.database.Table;
import hu.bmistore.model.User;

public class UserRepository extends Table {
    protected static UserRepository instance;

    public UserRepository() {
        super();

        this.setName("user");
        this.setFields("id, first_name, last_name, age, sex");
        this.setRecordModel("hu.bmistore.model.User");
    }

    /**
     * Inserts a new user
     *
     * @param user
     * @return
     */
    public boolean insert(User user) {
        String sql = String.format(
                "INSERT INTO %s (%s) VALUES (%s, '%s', '%s', %d, '%s');",
                this.getName(),
                this.getFields(),
                "null",
                user.getFirstName(),
                user.getLastName(),
                user.getAge(),
                user.getSex()
        );

        return this.adapter.execute(sql);
    }

    /**
     * Updates a user data
     *
     * @param user
     * @return
     */
    public boolean update(User user) {
        String sql = String.format(
                "UPDATE %s SET first_name='%s', last_name='%s', age=%d, sex='%s' WHERE id = %d",
                this.getName(),
                user.getFirstName(),
                user.getLastName(),
                user.getAge(),
                user.getSex(),
                user.getId()
        );

        return this.adapter.execute(sql);
    }

    /**
     * Deletes a user
     *
     * @param user
     * @return
     */
    public boolean delete(User user) {
        return this.delete(user.getId());
    }

    /**
     * Finds a user by id
     *
     * @param id
     *
     * @return
     */
    @Override
    public User findById(int id) {
        return (User) super.findById(id);
    }
    /**
     * Returns a new instance
     *
     * @return
     */
    public static UserRepository getInstance() {
        if (null == UserRepository.instance) {
            UserRepository.instance = new UserRepository();
        }

        return UserRepository.instance;
    }
}
