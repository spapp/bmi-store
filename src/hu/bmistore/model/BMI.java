/**
 * @author Sándor Papp <spapp@spappsite.hu>
 * @copyright 2015
 * @license http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @link https://bitbucket.org/spapp
 * @package hu.bmistore.model
 * @project bmi_store
 * @since 2015.05.14.
 */
package hu.bmistore.model;


public class BMI {
    public static final String BMI_VERY_SEVERELY_UNDERWEIGHT = "súlyos soványság";
    public static final String BMI_SEVERELY_UNDERWEIGHT = "mérsékelt soványság";
    public static final String BMI_UNDERWEIGHT = "enyhe soványság";
    public static final String BMI_NORMAL = "normál";
    public static final String BMI_OVERWEIGHT = "túlsúlyos";
    public static final String BMI_OBESE_CLASS_I = "I. fokú elhízás";
    public static final String BMI_OBESE_CLASS_II = "II. fokú elhízás";
    public static final String BMI_OBESE_CLASS_III = "III. fokú elhízás";

    protected int weight;
    protected int height;

    public BMI () {
    }

    public BMI (int weight, int height) {
        this.setWeight(weight);
        this.setHeight(height);
    }

    /**
     * Returns weight (kg)
     *
     * @return
     */
    public int getWeight () {
        return this.weight;
    }

    /**
     * Sets weight (kg)
     *
     * @param weight
     */
    public void setWeight (int weight) {
        this.weight = weight;
    }

    /**
     * Returns height (cm)
     *
     * @return
     */
    public int getHeight () {
        return this.height;
    }

    /**
     * Sets height (cm)
     *
     * @param height
     */
    public void setHeight (int height) {
        this.height = height;
    }

    /**
     * Returns BMI index
     *
     * @return
     *
     * @see <a href="http://en.wikipedia.org/wiki/Body_mass_index">Body mass index</a>
     */
    public float getBMI () {
        float height = (float) this.getHeight() / 100;

        return (float) this.getWeight() / (height * height);
    }

    /**
     * Returns BMI index as a text
     *
     * @return
     */
    public String getBMIString () {
        float bmi = this.getBMI();

        if (bmi < 16) {
            return BMI.BMI_VERY_SEVERELY_UNDERWEIGHT;
        } else if (bmi >= 16 && bmi < 17) {
            return BMI.BMI_SEVERELY_UNDERWEIGHT;
        } else if (bmi >= 17 && bmi < 18.5) {
            return BMI.BMI_UNDERWEIGHT;
        } else if (bmi >= 18.5 && bmi < 25) {
            return BMI.BMI_NORMAL;
        } else if (bmi >= 25 && bmi < 30) {
            return BMI.BMI_OVERWEIGHT;
        } else if (bmi >= 30 && bmi < 35) {
            return BMI.BMI_OBESE_CLASS_I;
        } else if (bmi >= 35 && bmi < 40) {
            return BMI.BMI_OBESE_CLASS_II;
        }

        return BMI.BMI_OBESE_CLASS_III;
    }

    /**
     * Returns as a string
     *
     * @return
     */
    @Override
    public String toString () {
        return String.format(
                "%.2f (%s)",
                this.getBMI(),
                this.getBMIString()
        );
    }
}
