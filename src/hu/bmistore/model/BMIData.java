/**
 * @author Sándor Papp <spapp@spappsite.hu>
 * @copyright 2015
 * @license http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @link https://bitbucket.org/spapp
 * @package hu.bmistore.model
 * @project bmi_store
 * @since 2015.05.14.
 */
package hu.bmistore.model;

import hu.database.Model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class BMIData implements Model {
    public static final String FORMAT_DATE_TIME = "yyyy-MM-dd HH:mm:ss";

    protected int id;
    protected int userId;
    protected int weight;
    protected int height;
    protected Date created;


    public BMIData () {
        this.setId(-1);
        this.setUserId(0);
        this.setWeight(0);
        this.setHeight(0);
    }

    public BMIData (int id, int userId, int weight, int height) {
        this.setId(id);
        this.setUserId(userId);
        this.setWeight(weight);
        this.setHeight(height);
    }

    public void load (ResultSet resultSet) throws SQLException {
        this.setId(resultSet.getInt("id"));
        this.setUserId(resultSet.getInt("user_id"));
        this.setWeight(resultSet.getInt("weight"));
        this.setHeight(resultSet.getInt("height"));
        this.setCreated(resultSet.getString("created"));
    }

    /**
     * Returns id
     *
     * @return
     */
    public int getId () {
        return this.id;
    }

    /**
     * Sets id
     *
     * @param id
     */
    public void setId (int id) {
        this.id = id;
    }

    /**
     * Returns user id
     *
     * @return
     */
    public int getUserId () {
        return this.userId;
    }

    /**
     * Sets user id
     *
     * @param userId
     */
    public void setUserId (int userId) {
        this.userId = userId;
    }

    /**
     * Returns weight
     *
     * @return
     */
    public int getWeight () {
        return this.weight;
    }

    /**
     * Sets weights
     *
     * @param weight
     */
    public void setWeight (int weight) {
        this.weight = weight;
    }

    /**
     * Returns height
     *
     * @return
     */
    public int getHeight () {
        return this.height;
    }

    /**
     * Sets height
     *
     * @param height
     */
    public void setHeight (int height) {
        this.height = height;
    }

    /**
     * Returns created date
     *
     * @return
     */
    public Date getCreated () {
        if (null == this.created) {
            this.created = Calendar.getInstance().getTime();
        }

        return this.created;
    }

    /**
     * Sets created date
     *
     * @param created
     */
    public void setCreated (Date created) {
        this.created = created;
    }

    /**
     * Sets created date from a stringű
     *
     * @param created
     */
    public void setCreated (String created) {
        SimpleDateFormat formatter = new SimpleDateFormat(BMIData.FORMAT_DATE_TIME);

        try {
            this.created = formatter.parse(created);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    /**
     * Returns created date as a string
     *
     * @return
     */
    public String getCreatedAsString () {
        SimpleDateFormat dateFormat = new SimpleDateFormat(BMIData.FORMAT_DATE_TIME);

        return dateFormat.format(this.getCreated());
    }

    /**
     * Returns as a string
     *
     * @return
     */
    @Override
    public String toString () {
        return String.format(
                "%d %d kg, %d cm (%s)",
                this.getId(),
                this.getWeight(),
                this.getHeight(),
                this.getCreatedAsString()
        );
    }

    /**
     * Returns TRUE if it is a new record
     *
     * @return
     */
    public boolean isNew () {
        return (this.id < 1);
    }

    /**
     * Returns TRUE if it is valid record
     *
     * @return
     */
    public boolean isValid () {
        return (this.userId > 0 && this.weight > 0 && this.weight < 500 && this.height > 0 && this.height < 300);
    }

    /**
     * Returns a new instance
     *
     * @return
     */
    public static BMIData create () {
        return new BMIData();
    }
}
