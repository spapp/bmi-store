/**
 * @author Sándor Papp <spapp@spappsite.hu>
 * @copyright 2015
 * @license http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @link https://bitbucket.org/spapp
 * @package hu.bmistore.model
 * @project bmi_store
 * @since 2015.05.14.
 */
package hu.bmistore.model;

import hu.database.Model;

import java.sql.ResultSet;
import java.sql.SQLException;

public class User implements Model {
    public static final String SEX_MALE = "male";
    public static final String SEX_FEMALE = "female";

    protected int id;
    protected String firstName;
    protected String lastName;
    protected int age;
    protected String sex;


    public User () {
        this.setId(-1);
        this.setFirstName("");
        this.setLastName("");
        this.setAge(0);
        this.setSex("");
    }

    public User (int id, String firstName, String lastName, int age, String sex) {
        this.setId(id);
        this.setFirstName(firstName);
        this.setLastName(lastName);
        this.setAge(age);
        this.setSex(sex);
    }

    public void load (ResultSet resultSet) throws SQLException {
        this.setId(resultSet.getInt("id"));
        this.setFirstName(resultSet.getString("first_name"));
        this.setLastName(resultSet.getString("last_name"));
        this.setAge(resultSet.getInt("age"));
        this.setSex(resultSet.getString("sex"));
    }

    /**
     * Returns user id
     *
     * @return
     */
    public int getId () {
        return id;
    }

    /**
     * Sets user id
     *
     * @param id
     */
    public void setId (int id) {
        this.id = id;
    }

    /**
     * Returns user first name
     *
     * @return
     */
    public String getFirstName () {
        return firstName;
    }

    /**
     * Sets user first name
     *
     * @param firstName
     */
    public void setFirstName (String firstName) {
        this.firstName = firstName;
    }

    /**
     * Returns user last name
     *
     * @return
     */
    public String getLastName () {
        return lastName;
    }

    /**
     * Sets user last name
     *
     * @return
     */
    public void setLastName (String lastName) {
        this.lastName = lastName;
    }

    /**
     * Returns user age
     *
     * @return
     */
    public int getAge () {
        return age;
    }

    /**
     * Sets user age
     *
     * @return
     */
    public void setAge (int age) {
        this.age = age;
    }

    /**
     * Returns user sex
     *
     * @return
     */
    public String getSex () {
        return sex;
    }

    /**
     * Sets user sex
     *
     * @return
     */
    public void setSex (String sex) {
        this.sex = sex;
    }

    /**
     * Returns user as a string
     *
     * @return
     */
    @Override
    public String toString () {
        return String.format(
                "%s %s (%d, %s)",
                this.getLastName(),
                this.getFirstName(),
                this.getAge(),
                this.getSex()
        );
    }

    /**
     * Returns TRUE if it is a new record
     *
     * @return
     */
    public boolean isNew () {
        return (this.id < 1);
    }

    /**
     * Returns TRUE if it is valid record
     *
     * @return
     */
    public boolean isValid () {
        boolean isValid = (this.firstName.length() > 0 && this.lastName.length() > 0 && this.age > 0);
        isValid = isValid && (this.sex == User.SEX_FEMALE || this.sex == User.SEX_MALE);

        return isValid;
    }

    /**
     * Returns a new user instance
     *
     * @return
     */
    public static User create () {
        return new User();
    }
}
