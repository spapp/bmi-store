/**
 * @author Sándor Papp <spapp@spappsite.hu>
 * @copyright 2015
 * @license http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @link https://bitbucket.org/spapp
 * @package hu.bmistore.view
 * @project bmi_store
 * @since 2015.05.14.
 */
package hu.bmistore.view;

import hu.bmistore.database.BMIDataRepository;
import hu.bmistore.model.BMI;
import hu.bmistore.model.BMIData;
import hu.view.AbstractForm;
import hu.view.Table;

import java.io.BufferedReader;
import java.io.PrintStream;
import java.util.ArrayList;

public class BMIDataForm extends AbstractForm {
    protected BMIData bmiData;

    public BMIDataForm (BufferedReader input, PrintStream output) {
        super(input, output);
    }

    /**
     * Displays the bmi data list
     */
    public void list () {
        String[] headers = {"ID", "Felhanáló ID", "Súly (kg)", "Magasság (cm)", "Test Tömeg Index", "Létrehozva"};
        int[] columnsWidth = {5, 5, 10, 13, 30, 30};
        ArrayList<BMIData> list = BMIDataRepository.getInstance().find();
        Table dataTable = new Table(headers, "Test Tömeg Index adatok", columnsWidth);

        for (BMIData bmiData : list) {
            String[] rowData = new String[6];
            BMI bmi = new BMI(bmiData.getWeight(), bmiData.getHeight());

            rowData[0] = String.format("%d", bmiData.getId());
            rowData[1] = String.format("%d", bmiData.getUserId());
            rowData[2] = String.format("%d", bmiData.getWeight());
            rowData[3] = String.format("%d", bmiData.getHeight());
            rowData[4] = bmi.toString();
            rowData[5] = bmiData.getCreatedAsString();

            dataTable.append(rowData);
        }

        this.output.println(dataTable.toString());
        this.waitFor();
    }

    /**
     * @inheritDoc
     */
    public void add () {
        this.output.println("Új TTI adat: ");
        this.output.println("----------------------------------");

        this.bmiData = BMIData.create();
        this.readData();

        if (this.isValid()) {
            this.save();
        } else {
            System.err.println("Invalid data form.");
        }
    }

    /**
     * @inheritDoc
     */
    public void modify () {
        int id = this.readIntValue("TTI adat id-ja:", 0, Integer.SIZE);
        this.bmiData = BMIDataRepository.getInstance().findById(id);

        this.output.println(String.format("TTI adat (%s) módosítás:", this.bmiData));
        this.output.println("----------------------------------");
        this.readData();

        if (this.isValid()) {
            this.save();
        } else {
            System.err.println("Invalid data form.");
        }
    }

    /**
     * @inheritDoc
     */
    public void delete () {
        int id = this.readIntValue("TTI adat id-ja:", 0, Integer.SIZE);
        this.bmiData = BMIDataRepository.getInstance().findById(id);

        if (BMIDataRepository.getInstance().delete(this.bmiData)) {
            this.output.println("Sikeres törlés!");
            this.output.println(String.format("TTI data: %s", this.bmiData));
        } else {
            this.output.println("Sikertelen törlés!");
        }

        this.waitFor();
    }

    /**
     * @return
     *
     * @inheritDoc
     */
    public boolean isValid () {
        return this.bmiData.isValid();
    }

    /**
     * Save current record
     */
    public void save () {
        boolean success;

        this.output.println("Mentés: " + this.bmiData);

        if (this.bmiData.isNew()) {
            success = BMIDataRepository.getInstance().insert(this.bmiData);
        } else {
            success = BMIDataRepository.getInstance().update(this.bmiData);
        }

        if (success) {
            this.output.println("A mentés sikeres!");
        } else {
            this.output.println("A mentés nem sikerült!");
        }

        this.waitFor();
    }

    /**
     * Asks for an data record details
     */
    protected void readData () {
        this.bmiData.setUserId(this.readIntValue("Felhasználó ID:", 1, Integer.SIZE));
        this.bmiData.setWeight(this.readIntValue("Súly (kg):", 1, 500));
        this.bmiData.setHeight(this.readIntValue("Magasság (cm):", 1, 300));
    }
}
