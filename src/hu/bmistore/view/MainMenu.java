/**
 * @author Sándor Papp <spapp@spappsite.hu>
 * @copyright 2015
 * @license http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @link https://bitbucket.org/spapp
 * @package hu.bmistore.view
 * @project bmi_store
 * @since 2015.05.14.
 */
package hu.bmistore.view;

import hu.view.Menu;
import hu.view.MenuListener;

import java.io.BufferedReader;
import java.io.PrintStream;

public class MainMenu extends Menu implements MenuListener {
    UserForm userForm;
    BMIDataForm dataForm;

    public MainMenu(BufferedReader input, PrintStream output) {
        super();

        String[] menu = {
                "Kilépés",
                "Felhasználó\tlista",
                "           \thozzáadás",
                "           \tmódosít",
                "           \ttörlés",
                "TTI adat\tlista",
                "        \thozzáadás",
                "        \tmódosít",
                "        \ttörlés",
        };

        this.setInputChannel(input);
        this.setOutputChannel(output);

        this.setMenu(menu);
        this.setTitle("Test Tömeg Index Store");
        this.setQuestion("Válassz:");

        this.userForm = new UserForm(this.input, this.output);
        this.dataForm = new BMIDataForm(this.input, this.output);

        this.listen(this);
    }

    /**
     * select menu listener
     *
     * @param menuIndex
     */
    public void selectMenu(int menuIndex) {
        switch (menuIndex) {
            case 0: // exit
                System.exit(0);
                break;
            case 1: // User list
                this.userForm.list();
                break;
            case 2: // User add
                this.userForm.add();
                break;
            case 3: // user mod
                this.userForm.modify();
                break;
            case 4: // user delete
                this.userForm.delete();
                break;
            case 5: // data list
                this.dataForm.list();
                break;
            case 6: // data add
                this.dataForm.add();
                break;
            case 7: // data mod
                this.dataForm.modify();
                break;
            case 8: // data delete
                this.dataForm.delete();
                break;
        }
    }

    public void showMenu() {

    }
}
