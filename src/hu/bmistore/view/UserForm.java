/**
 * @author Sándor Papp <spapp@spappsite.hu>
 * @copyright 2015
 * @license http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @link https://bitbucket.org/spapp
 * @package hu.bmistore.view
 * @project bmi_store
 * @since 2015.05.14.
 */
package hu.bmistore.view;

import hu.bmistore.database.UserRepository;
import hu.bmistore.model.User;
import hu.view.AbstractForm;
import hu.view.Table;

import java.io.BufferedReader;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;

public class UserForm extends AbstractForm {
    protected User user;
    protected HashMap<String, String> dictionary;

    public UserForm (BufferedReader input, PrintStream output) {
        super(input, output);

        this.dictionary = new HashMap<String, String>();

        this.dictionary.put(User.SEX_MALE, "férfi");
        this.dictionary.put(User.SEX_FEMALE, "nő");
    }

    /**
     * Displays the user list
     */
    public void list () {
        String[] headers = {"ID", "Vezetéknév", "Keresztnév", "Életkor", "Nem"};
        int[] columnsWidth = {5, 20, 20, 15, 10};
        ArrayList<User> list = UserRepository.getInstance().find();
        Table userTable = new Table(headers, "Felhasználók", columnsWidth);

        for (User user : list) {
            String[] rowData = new String[5];

            rowData[0] = String.format("%d", user.getId());
            rowData[1] = user.getLastName();
            rowData[2] = user.getFirstName();
            rowData[3] = String.format("%d", user.getAge());
            rowData[4] = this.dictionary.get(user.getSex());

            userTable.append(rowData);
        }

        this.output.println(userTable.toString());
        this.waitFor();
    }

    /**
     * @inheritDoc
     */
    public void add () {
        this.output.println("Új felhasználó adatai: ");
        this.output.println("----------------------------------");
        this.user = User.create();
        this.readData();

        if (this.isValid()) {
            this.save();
        } else {
            System.err.println("Invalid user form.");
        }
    }

    /**
     * @inheritDoc
     */
    public void modify () {
        int id = this.readIntValue("Felhasználó id-ja:", 0, Integer.SIZE);
        this.user = UserRepository.getInstance().findById(id);

        this.output.println(String.format("Felhasználó (%s) új adatai:", this.user));
        this.output.println("----------------------------------");

        this.readData();

        if (this.isValid()) {
            this.save();
        } else {
            System.err.println("Invalid user form.");
        }
    }

    /**
     * @inheritDoc
     */
    public void delete () {
        int id = this.readIntValue("Felhasználó id-ja:", 0, Integer.SIZE);
        this.user = UserRepository.getInstance().findById(id);

        if (UserRepository.getInstance().delete(this.user)) {
            this.output.println("Sikeres törlés!");
            this.output.println(String.format("Felhasználó: %s", this.user));
        } else {
            this.output.println("Sikertelen törlés!");
        }

        this.waitFor();
    }

    /**
     * @inheritDoc
     */
    public boolean isValid () {
        return this.user.isValid();
    }

    /**
     * Save current record
     */
    public void save () {
        boolean success;

        this.output.println("Mentés: " + this.user);

        if (this.user.isNew()) {
            success = UserRepository.getInstance().insert(this.user);
        } else {
            success = UserRepository.getInstance().update(this.user);
        }

        if (success) {
            this.output.println("A mentés sikeres!");
        } else {
            this.output.println("A mentés nem sikerült!");
        }

        this.waitFor();
    }

    /**
     * Asks for an data record details
     */
    protected void readData () {
        this.user.setFirstName(this.readStringValue("Keresztnév:", 1, 255));
        this.user.setLastName(this.readStringValue("Vezetéknév:", 1, 255));
        this.user.setAge(this.readIntValue("Életkor:", 1, 149));

        if (0 == this.readIntValue("nem (0 - férfi, 1 - nő):", 0, 1)) {
            this.user.setSex(User.SEX_MALE);
        } else {
            this.user.setSex(User.SEX_FEMALE);
        }
    }
}
