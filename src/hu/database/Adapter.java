/**
 * @author Sándor Papp <spapp@spappsite.hu>
 * @copyright 2015
 * @license http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @link https://bitbucket.org/spapp
 * @package hu.database
 * @project bmi_store
 * @since 2015.05.14.
 */
package hu.database;

import java.util.ArrayList;

/**
 * Interface for RDMS
 */
public interface Adapter {
    /**
     * For not select query
     *
     * @param query SQL query
     *
     * @return Returns TRUE if the query is success
     */
    public boolean execute (String query);

    /**
     * For select query
     *
     * @param query       SQL query
     * @param recordModel record model class name
     *
     * @return returns response list
     *
     * @throws ClassNotFoundException
     */
    public ArrayList<Model> select (String query, String recordModel) throws ClassNotFoundException;
}
