/**
 * @author Sándor Papp <spapp@spappsite.hu>
 * @copyright 2015
 * @license http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @link https://bitbucket.org/spapp
 * @package hu.database
 * @project bmi_store
 * @since 2015.05.14.
 */
package hu.database;

import java.sql.ResultSet;
import java.sql.SQLException;

public interface Model {
    /**
     * Creates a record model from a recordset
     *
     * @param resultSet
     *
     * @throws SQLException
     */
    public void load (ResultSet resultSet) throws SQLException;

    /**
     * Returns TRUE if the record is valid
     *
     * @return
     */
    public boolean isValid ();

    /**
     * Returns TRUE if the record is a new record
     *
     * @return
     */
    public boolean isNew ();
}
