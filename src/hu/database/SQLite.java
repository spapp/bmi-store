/**
 * @author Sándor Papp <spapp@spappsite.hu>
 * @copyright 2015
 * @license http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @link https://bitbucket.org/spapp
 * @package hu.database
 * @project bmi_store
 * @since 2015.05.14.
 */
package hu.database;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class SQLite implements Adapter {
    public static final String JDBC_DRIVER = "org.sqlite.JDBC";
    public static final String DB_URL = "jdbc:sqlite:";
    /**
     * Database file uri
     */
    protected String database;

    /**
     * @param database database file uri
     */
    public SQLite (String database) {
        this.database = database;
    }

    /**
     * @inheritDoc
     */
    public boolean execute (String query) {
        Connection connection;
        Statement statement;
        boolean success = false;

        try {
            connection = this.getConnection();
            statement = connection.createStatement();

            statement.executeUpdate(query);
            statement.close();
            connection.commit();
            connection.close();

            success = true;
        } catch (Exception e) {
            System.err.println(query);
            System.err.println(e);
            e.printStackTrace();
        }

        return success;
    }

    /**
     * @inheritDoc
     */
    public ArrayList<Model> select (String query, String recordModel) throws ClassNotFoundException {
        Connection connection;
        Statement statement;
        ArrayList<Model> response = new ArrayList<Model>();
        Model record;

        try {
            Class modelClass = Class.forName(recordModel); // Dynamically load the class
            connection = this.getConnection();
            statement = connection.createStatement();

            ResultSet rs = statement.executeQuery(query);

            while (rs.next()) {
                record = (Model) modelClass.newInstance(); // Dynamically instantiate it
                record.load(rs);
                response.add(record);
            }

            rs.close();
            statement.close();
            connection.close();
        } catch (Exception e) {
            System.err.println(query);
            System.err.println(e);
            e.printStackTrace();
        }

        return response;
    }

    /**
     * Returns SQLite connection string
     *
     * @return connection string
     */
    protected String getConnectionString () {
        return SQLite.DB_URL + this.database;
    }

    /**
     * Returns a SQLite connection
     *
     * @return SQLite connection
     *
     * @throws Exception
     */
    protected Connection getConnection () throws Exception {
        Connection connection = null;

        Class.forName(SQLite.JDBC_DRIVER);
        connection = DriverManager.getConnection(this.getConnectionString());
        connection.setAutoCommit(false);

        return connection;
    }
}
