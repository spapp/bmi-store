/**
 * @author Sándor Papp <spapp@spappsite.hu>
 * @copyright 2015
 * @license http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @link https://bitbucket.org/spapp
 * @package hu.database
 * @project bmi_store
 * @since 2015.05.14.
 */
package hu.database;

import java.util.ArrayList;

public abstract class Table {
    protected String name;
    protected String fields;
    protected String recordModel;

    protected Adapter adapter;

    public void Table () {
    }

    /**
     * Returns table name
     *
     * @return
     */
    public String getName () {
        return this.name;
    }

    /**
     * Sets the table name
     *
     * @param name
     */
    public void setName (String name) {
        this.name = name;
    }

    /**
     * Returns table field list
     *
     * @return
     */
    public String getFields () {
        return this.fields;
    }

    /**
     * Sets table field list
     *
     * @param fields
     */
    public void setFields (String fields) {
        this.fields = fields;
    }

    /**
     * Returns record model class name
     *
     * @return
     */
    public String getRecordModel () {
        return this.recordModel;
    }

    /**
     * Sets the record model class name
     *
     * @param recordModel
     */
    public void setRecordModel (String recordModel) {
        this.recordModel = recordModel;
    }

    /**
     * Retuns a SQL select response
     *
     * @param where
     *
     * @return
     *
     * @see Table#find()
     */
    public ArrayList find (String where) {
        String sql = String.format(
                "SELECT %s FROM %s WHERE %s",
                this.getFields(),
                this.getName(),
                where
        );

        try {
            return this.adapter.select(sql, this.recordModel);
        } catch (ClassNotFoundException e) {
            System.err.println(sql);
            System.err.println(e);
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Retuns a SQL select response
     *
     * @return
     *
     * @see Table#find(String where)
     */
    public ArrayList find () {
        String sql = String.format(
                "SELECT %s FROM %s",
                this.getFields(),
                this.getName()
        );

        try {
            return this.adapter.select(sql, this.recordModel);
        } catch (ClassNotFoundException e) {
            System.err.println(sql);
            System.err.println(e);
            e.printStackTrace();
        } catch (Exception e) {
            System.err.println(sql);
            System.err.println(e);
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Returns a record by id
     *
     * @param id
     *
     * @return
     */
    public Model findById (int id) {
        String where = String.format("id = %d", id);

        ArrayList response = this.find(where);

        if (response.size() > 0) {
            return (Model) response.get(0);
        }

        return null;
    }

    /**
     * Delete a record by id
     *
     * @param id
     *
     * @return
     */
    public boolean delete (int id) {
        String sql = String.format(
                "DELETE FROM %s WHERE id = %d",
                this.getName(),
                id
        );

        return this.adapter.execute(sql);
    }

    /**
     * Sets a RDBMS adapter
     *
     * @param adapter
     */
    public void setAdapter (Adapter adapter) {
        this.adapter = adapter;
    }

    /**
     * Returns current RDBMS adapter
     *
     * @return
     */
    public Adapter getAdapter () {
        return this.adapter;
    }
}
