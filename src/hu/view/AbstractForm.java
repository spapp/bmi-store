/**
 * @author Sándor Papp <spapp@spappsite.hu>
 * @copyright 2015
 * @license http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @link https://bitbucket.org/spapp
 * @package hu.view
 * @project bmi_store
 * @since 2015.05.14.
 */
package hu.view;

import java.io.BufferedReader;
import java.io.PrintStream;

public abstract class AbstractForm {
    protected PrintStream output;
    protected BufferedReader input;

    /**
     * @param input
     * @param output
     */
    public AbstractForm (BufferedReader input, PrintStream output) {
        this.input = input;
        this.output = output;
    }

    /**
     * Reads a int value from AbstractForm#input
     *
     * @param title
     * @param minimum valid minimum value
     * @param maximum valid maximum value
     *
     * @return
     */
    protected int readIntValue (String title, int minimum, int maximum) {
        int value;
        String errorMessage = String.format("Az értéke %d és %d közzé kell essen!", minimum, maximum);

        this.output.println(title);

        do {
            try {
                value = Integer.parseInt(this.input.readLine());

                if (value < minimum || value > maximum) {
                    throw new Exception(errorMessage);
                }
                break;
            } catch (Exception exc) {
                this.output.println(errorMessage);
            }
        } while (true);

        return value;
    }

    /**
     * Reads a string value from AbstractForm#input
     *
     * @param title
     * @param minLength string valid minimum length
     * @param maxLength string valid maximum length
     *
     * @return
     */
    protected String readStringValue (String title, int minLength, int maxLength) {
        String value;
        String errorMessage = String.format("A hossza %d - %d karakter lehet!", minLength, maxLength);

        this.output.println(title);

        do {
            try {
                value = this.input.readLine();

                if (value.length() < minLength || value.length() > maxLength) {
                    throw new Exception(errorMessage);
                }

                break;
            } catch (Exception exc) {
                this.output.println(errorMessage);
            }
        } while (true);

        return value;
    }

    /**
     * Waiting
     */
    protected void waitFor () {
        this.output.println("A folytatáshoz üss le egy billentyűt.");

        try {
            this.input.readLine();
        } catch (Exception e) {

        }
    }

    /**
     * Displays the data set
     */
    public abstract void list ();

    /**
     * Asks for a new record details and save it
     */
    public abstract void add ();

    /**
     * Asks for an existing record details and save it
     */
    public abstract void modify ();

    /**
     * Deletes an existing record
     */
    public abstract void delete ();

    /**
     * Returns TRUE if the data is valid
     *
     * @return
     */
    public abstract boolean isValid ();

    /**
     * Save the current record
     */
    public abstract void save ();
}

