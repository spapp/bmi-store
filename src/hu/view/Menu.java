/**
 * @author Sándor Papp <spapp@spappsite.hu>
 * @copyright 2015
 * @license http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @link https://bitbucket.org/spapp
 * @package hu.view
 * @project bmi_store
 * @since 2015.05.14.
 */
package hu.view;


import java.io.BufferedReader;
import java.io.PrintStream;

public class Menu {
    protected PrintStream output;
    protected BufferedReader input;
    protected String[] menu;
    protected String title;
    protected String question;

    public Menu () {
    }

    /**
     * Sets the menu title
     *
     * @param title
     */
    public void setTitle (String title) {
        this.title = title;
    }

    /**
     * Sets the menu question
     *
     * @param question
     */
    public void setQuestion (String question) {
        this.question = question;
    }

    /**
     * Sets menu items
     *
     * @param menu
     */
    public void setMenu (String[] menu) {
        this.menu = menu;
    }

    /**
     * Sets menu imput channel
     *
     * @param input
     */
    public void setInputChannel (BufferedReader input) {
        this.input = input;
    }

    /**
     * Sets the menu output channels
     *
     * @param output
     */
    public void setOutputChannel (PrintStream output) {
        this.output = output;
    }

    /**
     * Sets the menu listener
     *
     * @param listener
     */
    public void listen (MenuListener listener) {
        String line;
        int menuIndex = -1;

        do {
            this.print();

            try {
                line = this.input.readLine();
                menuIndex = Integer.parseInt(line);
                listener.selectMenu(menuIndex);
            } catch (Exception e) {
                menuIndex = -1;
            }

        } while (menuIndex != 0);

        listener.selectMenu(menuIndex);
    }

    /**
     * Prints menu
     */
    public void print () {
        String hr = String.format("%35s", "").replace(' ', '-');
        this.output.println("");
        this.output.println(this.title);
        this.output.println(hr);

        for (int i = 0; i < this.menu.length; i++) {
            this.output.println(String.format("%3d - %s", i, this.menu[i]));
        }

        this.output.println(hr);
        this.output.println(this.question);
    }
}
