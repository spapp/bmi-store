/**
 * @author Sándor Papp <spapp@spappsite.hu>
 * @copyright 2015
 * @license http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @link https://bitbucket.org/spapp
 * @package hu.view
 * @project bmi_store
 * @since 2015.05.14.
 */
package hu.view;

public interface MenuListener {
    /**
     * Runs if the menu was selected
     *
     * @param index
     */
    public void selectMenu (int index);

    /**
     * Runs if the menu was displayed
     */
    public void showMenu ();
}
