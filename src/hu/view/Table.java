/**
 * @author Sándor Papp <spapp@spappsite.hu>
 * @copyright 2015
 * @license http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @link https://bitbucket.org/spapp
 * @package hu.view
 * @project bmi_store
 * @since 2015.05.14.
 */
package hu.view;


import java.util.ArrayList;

public class Table {
    public static final String DEFAULT_CAPTION = "";
    public static final int DEFAULT_COLUMN_WIDTH = 20;

    public static final String SEPARATOR_LEFT = "| ";
    public static final String SEPARATOR_RIGHT = " |";
    public static final String SEPARATOR_MIDDLE = " | ";

    public static final int ALIGN_LEFT = 0;
    public static final int ALIGN_RIGHT = 1;
    public static final int ALIGN_CENTER = 2;

    protected String caption;
    protected String[] columns;
    protected ArrayList<String[]> data;

    protected int[] columnWidth;

    public Table (String[] columns, String caption, int[] columnWidth) {
        this.setColumnWidth(columnWidth);
        this.setData(new ArrayList<String[]>());
        this.setColumns(columns);
        this.setCaption(caption);
    }

    public Table (String[] columns, String caption) {
        this.setData(new ArrayList<String[]>());
        this.setColumns(columns);
        this.setCaption(caption);
    }

    public Table (String[] columns) {
        this.setData(new ArrayList<String[]>());
        this.setColumns(columns);
        this.setCaption(Table.DEFAULT_CAPTION);
    }

    /**
     * Returns table caption
     *
     * @return
     */
    public String getCaption () {
        return this.caption;
    }

    /**
     * Sets table caption
     *
     * @param caption
     */
    public void setCaption (String caption) {
        this.caption = caption;
    }

    /**
     * Returns table columns
     *
     * @return
     */
    public String[] getColumns () {
        return this.columns;
    }

    /**
     * Sets table columns
     *
     * @param columns
     */
    public void setColumns (String[] columns) {
        this.columns = columns;
    }

    /**
     * Returns table data
     *
     * @return
     */
    public ArrayList<String[]> getData () {
        return this.data;
    }

    /**
     * Sets table data
     *
     * @param data
     */
    public void setData (ArrayList<String[]> data) {
        this.data = data;
    }

    /**
     * Appands a new row to table
     *
     * @param row
     */
    public void append (String[] row) {
        this.data.add(row);
    }

    /**
     * Returns columns width
     *
     * @return
     */
    public int[] getColumnWidth () {
        return this.columnWidth;
    }

    /**
     * Returns a column width
     *
     * @param columnIndex
     *
     * @return
     */
    public int getColumnWidth (int columnIndex) {
        int columnWidth = Table.DEFAULT_COLUMN_WIDTH;

        if (null != this.columnWidth && this.columnWidth.length - 1 >= columnIndex) {
            columnWidth = this.columnWidth[columnIndex];
        }

        if (this.columns[columnIndex].length() > columnWidth) {
            columnWidth = this.columns[columnIndex].length();
        }

        return columnWidth;
    }

    /**
     * Sets columns width
     *
     * @param columnWidth
     */
    public void setColumnWidth (int[] columnWidth) {
        this.columnWidth = columnWidth;
    }

    /**
     * Returns table as a string
     *
     * @return
     */
    @Override
    public String toString () {
        StringBuffer table = new StringBuffer();

        table.append(this.getHeader());

        for (String[] rowData : this.data) {
            table.append("\n");
            table.append(this.getRow(rowData, Table.ALIGN_RIGHT));
        }

        table.append("\n");
        table.append(this.getHr());
        table.append("\n");

        return table.toString();
    }

    /**
     * Returns row width
     *
     * @return
     */
    public int getRowWidth () {
        int rowWidth = 0;

        for (int i = 0; i < this.columns.length; i++) {
            rowWidth += this.getColumnWidth(i);
        }

        rowWidth += Table.SEPARATOR_LEFT.length();
        rowWidth += Table.SEPARATOR_MIDDLE.length() * (this.columns.length - 1);
        rowWidth += Table.SEPARATOR_RIGHT.length();

        return rowWidth;
    }

    /**
     * Returns a table row as string
     *
     * @param data
     * @param align
     *
     * @return
     */
    protected String getRow (String[] data, int align) {
        StringBuffer row = new StringBuffer();

        row.append(Table.SEPARATOR_LEFT);

        for (int i = 0; i < data.length; i++) {
            if (i > 0) {
                row.append(Table.SEPARATOR_MIDDLE);
            }

            switch (align) {
                case Table.ALIGN_CENTER:
                    row.append(this.padBoth(data[i], this.getColumnWidth(i)));
                    break;
                case Table.ALIGN_RIGHT:
                    row.append(this.padRight(data[i], this.getColumnWidth(i)));
                    break;
                case Table.ALIGN_LEFT:
                default:
                    row.append(this.padLeft(data[i], this.getColumnWidth(i)));
            }
        }

        row.append(Table.SEPARATOR_RIGHT);

        return row.toString();
    }

    /**
     * Returns table header as string
     *
     * @return
     */
    protected String getHeader () {
        StringBuffer header = new StringBuffer();

        // caption
        header.append(this.padBoth(this.caption, this.getRowWidth()));
        header.append("\n");
        // hr
        header.append(this.getHr());
        header.append("\n");
        // head
        header.append(this.getRow(this.columns, Table.ALIGN_CENTER));
        header.append("\n");
        // hr
        header.append(this.getHr());

        return header.toString();
    }

    /**
     * Pad right a string to a certain length with space
     *
     * @param text
     * @param length
     *
     * @return
     */
    protected String padRight (String text, int length) {
        return String.format("%1$-" + length + "s", text);
    }

    /**
     * Pad left a string to a certain length with space
     *
     * @param text
     * @param length
     *
     * @return
     */
    protected String padLeft (String text, int length) {
        return String.format("%" + length + "s", text);
    }

    /**
     * Pad a string to a certain length with space
     *
     * @param text
     * @param length
     *
     * @return
     */
    protected String padBoth (String text, int length) {
        int padLeft = (int) ((length - text.length()) / 2);
        String t = this.padLeft(text, padLeft + text.length());

        return this.padRight(t, length);
    }

    /**
     * Returns a line
     *
     * @return
     */
    protected String getHr () {
        return String.format(
                "%" + this.getRowWidth() + "s",
                ""
        ).replace(' ', '-');
    }
}
